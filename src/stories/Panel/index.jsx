import React from "react";
import { string, node, oneOfType, arrayOf } from "prop-types";

import styles from "./index.module.scss";
import cn from "classnames";

const Panel = ({ children, classname, ...props }) => {
  return (
    <div className={cn(styles.base, classname)} {...props}>
      {children}
    </div>
  );
};

Panel.propTypes = {
  children: oneOfType([arrayOf(node), node]),
  classname: string,
};
Panel.defaultProps = {
  color: "",
};

export default Panel;
