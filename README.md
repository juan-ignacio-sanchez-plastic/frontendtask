You can find the task [here](https://docs.google.com/document/d/1xVeHSyR3_K6ZNwaQHAIGp9n0mIPEzvQZBX2Qyx4yM9E/edit?usp=sharing).

# Dev

1. Install dependencies: `yarn`
2. Launch storybook: `yarn run storybook`
3. Add components to `src/stories`
